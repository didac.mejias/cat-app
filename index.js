window.onload = function () {

  const data = {
    currentCat: null,
    adminForm: false,
    cats: [
      {
        id: 0,
        name: 'Leia',
        image: 'https://cataas.com/cat',
        counter: 0
      }, {
        id: 1,
        name: 'Puffy',
        image: 'https://cataas.com/cat',
        counter: 0
      }, {
        id: 2,
        name: 'Darky',
        image: 'https://cataas.com/cat',
        counter: 0
      }, {
        id: 3,
        name: 'Turing',
        image: 'https://cataas.com/cat',
        counter: 0
      }, {
        id: 4,
        name: 'Ada',
        image: 'https://cataas.com/cat',
        counter: 0
      }
    ]
  }

  const octopus = {
    init: function () {
      this.cat.init();
      this.adminForm.init();
    },
    cat: {
      init: function () {
        data.currentCat = data.cats[0];
        this.createList();
        this.showCurrentCat();
      },
      createList: function () {
        catList.init(data.cats);
      },
      setCurrentCat: function (id) {
        data.currentCat = this.findCat(id);
      },
      showCurrentCat: function () {
        catDetail.init(data.currentCat);
      },
      updateCatDetails: function () {
        catDetail.update(data.currentCat);
      },
      findCat: function (id) {
        return data.cats.find(cat => cat.id === id)
      },
      addOne: function (id) {
        const cat = this.findCat(id);
        cat.counter++;
      }
    },
    adminForm: {
      init: function () {
        this.createAdminForm();
      },
      createAdminForm: function () {
        adminForm.init(data.currentCat);
      },
      saveChanges: function (newData) {
        const { newName, newUrl, newCounter } = newData;
        const cat = octopus.cat.findCat(data.currentCat.id)

        cat.name = newName;
        cat.image = newUrl;
        cat.counter = newCounter;
        this.closeAdminForm();

        catList.clear();
        octopus.cat.createList();
        octopus.cat.showCurrentCat(data.currentCat);
      },
      toggleAdminForm: function () {
        data.adminForm = !data.adminForm;
        data.adminForm ? this.openAdminForm() : this.closeAdminForm();
      },
      openAdminForm: function () {
        data.adminForm = true;
        adminForm.render(data.currentCat);
        viewActions.show(adminForm.getAdminForm(), 'flex');
      },
      closeAdminForm: function () {
        data.adminForm = false;
        adminForm.clear();
        viewActions.hide(adminForm.getAdminForm());
      }
    }
  }

  const catList = {
    init: function (cats) {
      for (let i = 0; i < cats.length; i++) {
        this.create(cats[i]);
      }
    },
    clear: function () {
      const elem = viewActions.getByClass('cat-list');
      elem.innerHTML = '';
    },
    create: function (cat) {
      const li = document.createElement('li');
      const span = document.createElement('span');
      li.id = cat.id;
      span.textContent = cat.name;

      viewActions.appendChilds(li, [span]);
      li.addEventListener('click', function () {
        octopus.cat.setCurrentCat(cat.id);
        octopus.cat.showCurrentCat();
        octopus.adminForm.closeAdminForm()
      });

      this.render(li);
    },
    render: function (li) {
      const elem = viewActions.getByClass('cat-list');

      viewActions.appendChilds(elem, [li]);
    }
  }

  const catDetail = {
    init: function (cat) {
      this.create(cat);
    },
    create: function (cat) {
      const counter = document.createElement('span');
      const name = document.createElement('span');
      const img = document.createElement('img');
      counter.textContent = cat.counter;
      counter.className = 'cat-counter';
      name.textContent = cat.name;
      img.src = cat.image;
      img.className = 'cat-image';

      img.addEventListener('click', () => {
        octopus.cat.addOne(cat.id);
        octopus.cat.updateCatDetails();
      });

      this.render(img, name, counter);
    },
    clear: function (elem) {
      elem.innerHTML = '';
    },
    update: function (cat) {
      const catCounter = viewActions.getByClass('cat-counter');
      catCounter.innerHTML = cat.counter;
    },
    render: function (img, name, counter) {
      const elem = viewActions.getByClass('selected-cat');
      this.clear(elem);

      viewActions.appendChilds(elem, [img, name, counter]);
    }
  }

  const adminForm = {
    init: function () {
      const adminButton = viewActions.getByClass('form-button');
      const cancelButton = viewActions.getByClass('cancel-button');
      const saveButton = viewActions.getByClass('save-button');

      adminButton.addEventListener('click', function () {
        octopus.adminForm.toggleAdminForm();
      });

      cancelButton.addEventListener('click', function () {
        octopus.adminForm.closeAdminForm();
      })

      saveButton.addEventListener('click', function () {
        const [nameInput, urlInput, counterInput] = adminForm.getInputs()
        const newName = nameInput.value;
        const newUrl = urlInput.value;
        const newCounter = counterInput.value;

        octopus.adminForm.saveChanges({ newName, newUrl, newCounter })
      })
    },
    getInputs: function () {
      const nameInput = document.querySelectorAll("[name^=name")[0];
      const urlInput = document.querySelectorAll("[name^=url")[0];
      const counterInput = document.querySelectorAll("[name^=counter")[0];

      return [nameInput, urlInput, counterInput];
    },
    getAdminForm: function () {
      return viewActions.getByClass('admin-form-content');
    },
    clear: function () {
      viewActions.clearInputs(this.getInputs());
    },
    render: function (cat) {
      const [nameInput, urlInput, counterInput] = this.getInputs();

      nameInput.value = cat.name;
      urlInput.value = cat.image;
      counterInput.value = cat.counter;
    }
  }

  const viewActions = {
    show: function (elem, type) {
      elem.style.display = type;
    },
    hide: function (elem) {
      elem.style.display = 'none';
    },
    clearInputs: function (inputs) {
      for (let i = 0; i < inputs.length; i++) {
        inputs[i].value = '';
      }
    },
    appendChilds: function (elem, childs) {
      for (let i = 0; i < childs.length; i++) {
        elem.appendChild(childs[i]);
      };
    },
    getByClass: function (className) {
      return document.getElementsByClassName(className)[0]
    }
  }

  octopus.init();
}